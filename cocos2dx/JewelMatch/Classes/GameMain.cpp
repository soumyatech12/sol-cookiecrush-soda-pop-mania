//
//  GameMain.cpp
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#include "GameMain.h"
#include "SimpleAudioEngine.h"
#include "NDKHelper.h"
#include "Utility.h"

//#include "AdMobX.h"

//#include "Wrapper.h"

#define FONT_NAME  "coolvetica.ttf"

CCScene* GameMain::scene()
{
    CCScene* scene= CCScene::create();
    GameMain * layer = new GameMain;
    layer->init();
    scene->addChild(layer);
    return scene;
}


void GameMain::tournamentEnded(){
}

bool GameMain::init(){
	isNxtPeer = false;
    if(! CCLayer::init())
        return false;
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menu.mp3",true);
//    CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameMain::startBtnHandler), "startGameWithNP", NULL);  /// for next peer call
//    CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(GameMain::removeAdsFinished), "removeAdsPurchaseSucess", NULL);

    NDKHelper::AddSelector("GameSceneSelectors",
            "startGame",
            callfuncND_selector(GameMain::startGame),
            this);
    float ip5OffSet;
    //////////////////////////////
    touchTargetAdded = false;
    this->setKeypadEnabled(true);
    
    sd = SharedData::getSharedInstance();
    ws=  CCDirector::sharedDirector()->getWinSize();
    
    gameTime=1800;
    
    if (ws.height==568) {
        
        hNum=7;
        vNum=10;
        ip5OffSet = 50.0;
    }
    
    else{
        hNum=7;
        vNum=8;
        ip5OffSet = 0.0;
    }
    
    
    gemWid=45*sd->scaleFactorX;
    
    gem2DArr=CCArray::create();
    gem2DArr->retain();
    
    for (int j=0; j<vNum; j++)
    {
        CCArray *hArr=CCArray::create();
        for (int i=0; i<hNum; i++)
        {
            hArr->addObject(CCString::create("em"));
        }
        gem2DArr->addObject(hArr);
    }
    

    
    
    mainMenuSprite=CCSprite::create();
    this->addChild(mainMenuSprite);
    mainMenuSprite->retain();
    
    
    
    mainMengBG= CCSprite::create(Utility::getImage("mainMenuBG").c_str());
//    mainMengBG.scaleX = 1.2;
//    mainMengBG.scaleY = sd.imgScaleFactorY;
    mainMengBG->setAnchorPoint(ccp(0,1));
    mainMengBG->setPosition(ccp(0,ws.height));
    mainMenuSprite->addChild(mainMengBG);
    
    rateBtn= createButtonWithFile(Utility::getImage("rate_me").c_str(), menu_selector(GameMain::rateBtnHandlerDirect));
        mainMenuSprite->addChild(rateBtn);

    
    startBtn= createButtonWithFile(Utility::getImage("startBtn").c_str(), menu_selector(GameMain::startBtnHandler));
    startBtn->setPositionY(rateBtn->getPositionY()-startBtn->getContentSize().height/32-40.0);
    mainMenuSprite->addChild(startBtn);

    
//    /////// junaid //////////
    multiPlayerBtn = createButtonWithFile(Utility::getImage("multiBtn").c_str(), menu_selector(GameMain::multiPlayerBtnHandler));
    multiPlayerBtn->setPositionY(startBtn->getPositionY()-multiPlayerBtn->getContentSize().height/32-40.0);
    mainMenuSprite->addChild(multiPlayerBtn);
  
    leaderBtn = createButtonWithFile(Utility::getImage("leaderBtn").c_str(), menu_selector(GameMain::leaderBtnHandler));
    leaderBtn->setPositionY(multiPlayerBtn->getPositionY()-leaderBtn->getContentSize().height/32-40.0);
    mainMenuSprite->addChild(leaderBtn);
    
    moreAppsBtn=createButtonWithFile(Utility::getImage("moreAppsBtn").c_str(), menu_selector(GameMain::moreAppsBtnHandler));
    moreAppsBtn->setPositionY(leaderBtn->getPositionY()-moreAppsBtn->getContentSize().height/32-40.0);
    mainMenuSprite->addChild(moreAppsBtn);

//
//    removAdBtn = createButtonWithFile("upgradeBtn.png", menu_selector(GameMain::removAdBtnHandler));
//    removAdBtn->setPositionY(multiPlayerBtn->getPositionY()-removAdBtn->getContentSize().height/32-40.0);
//    removAdBtn->setPosition(ccp(250.0,ws.height-25));
//    mainMenuSprite->addChild(removAdBtn);
//
//    restoreBtn = createButtonWithFile("restoreBtn.png" , menu_selector(GameMain::restorBtnHandler));
//    restoreBtn->setPosition(ccp(70,ws.height-25));
//    mainMenuSprite->addChild(restoreBtn);
//    //////////////////////////
//    
//    
//    
    inGame=CCSprite::create();
    inGame->retain();
    inGame->setPosition(ccp(0,ws.height));
    CCSprite *IGBG=CCSprite::create(Utility::getImage("inGameBG2").c_str());
    IGBG->setAnchorPoint(ccp(0,1));
//

//    
//    
    gemsCMC=CCSprite::create();
    gemsCMC->retain();
    gemsCMC->setPosition(ccp(gemWid/2+2+1,-70*sd->scaleFactorY));
    inGame->addChild(gemsCMC);
    gemsCMC->setParent(inGame);
    inGame->addChild(IGBG,-1);
    
    inGame->setVisible(false);
    this->addChild(inGame);
   
    backBtn=createButtonWithFile(Utility::getImage("backBtn").c_str(), menu_selector(GameMain::backHandler));
    backBtn->setPosition(ccp(ws.width-40*sd->scaleFactorX,-20*sd->scaleFactorY)); //// 74
    inGame->addChild(backBtn);

//    ///////// junaid ////////
    scoreTextTTF = CCLabelTTF::create("99999999999", FONT_NAME, 25.0*sd->scaleFactorY);
    scoreTextTTF->setAnchorPoint(ccp(0,0.5));
    scoreTextTTF->setString("0");
    scoreTextTTF->setPosition(ccp(100*sd->scaleFactorX,-18*sd->scaleFactorY));  /// 74
//    ////////////////////////
//
    timeBar=CCSprite::create(Utility::getImage("timeBar").c_str());
    timeBar->setAnchorPoint(ccp(0,1));
    timeBar->setPosition(ccp(0,-40));  // 90
    timeBar->setScaleX(CCDirector::sharedDirector()->getWinSizeInPixels().width/10);
    inGame->addChild(timeBar);
//
    inGame->addChild(scoreTextTTF);
//
//
    this->addSingleTouch();
//
//
    gameOver=CCSprite::create();
    gameOver->retain();
//
    CCSprite *gameOverBG=CCSprite::create(Utility::getImage("gameOver").c_str());
    gameOverBG->setAnchorPoint(ccp(0,1));

    gameOver->addChild(gameOverBG);
//
    gameOver->setPosition(ccp(0,ws.height));
//
    this->addChild(gameOver);
//
//
//
//    ///////// junaid ////////
    scoreTextTTF2 = CCLabelTTF::create("99999999999",FONT_NAME ,35.0*sd->scaleFactorY);
    scoreTextTTF2-> setAnchorPoint(ccp(0.5,0.5));
    scoreTextTTF2->setScale(1.5);
    scoreTextTTF2->setString("000000000");
//

        scoreTextTTF2->setPosition(ccp(ws.width/2 ,-185));
//    }
//    /////////////////////////
//

    gameOver->addChild(scoreTextTTF2);
    gameOver->setVisible(false);
//
//
//    freeBtn = createButtonWithFile("freeBtn.png", menu_selector(GameMain::freeBtnHandler));
//    freeBtn->setPosition(ccp(ws.width/2,-340*sd->scaleFactorY));
//    gameOver->addChild(freeBtn);
//
    okBtn=createButtonWithFile(Utility::getImage("okBtn").c_str(), menu_selector(GameMain::okHandler));
    okBtn->setPosition(ccp(ws.width/2,-400*sd->scaleFactorY));
    gameOver->addChild(okBtn);
//
    moreGamesBtnGameOver  = createButtonWithFile(Utility::getImage("moreAppsBtn").c_str(), menu_selector(GameMain::moreAppsBtnHandler));
    moreGamesBtnGameOver->setPosition(ccp(ws.width/2,-280*sd->scaleFactorY));
    gameOver->addChild(moreGamesBtnGameOver);
//}
    return true;
}

void GameMain::dashboardDisappear(){
	CCLog("dashboardDisappear");
	 CCUserDefault::sharedUserDefault()->setBoolForKey("nextpeerActive", false);
}

void GameMain::dashboardAppear(){
	CCLog("dashboardAppear");
	CCUserDefault::sharedUserDefault()->setBoolForKey("nextpeerActive", true);
}

CCMenu * GameMain::createButtonWithFile(const char * fileName, SEL_MenuHandler _sel)
{
    CCSprite *selectedBtn=CCSprite::create(fileName);
    selectedBtn->setPosition(ccp(1,-1));
    CCMenuItemSprite * sp = CCMenuItemSprite::create(CCSprite::create(fileName), selectedBtn, this, _sel);
    CCMenu * menu = CCMenu::create(sp,NULL);
    return menu;
}

#pragma mark button handlers
void GameMain::startBtnHandler()
{
	 if(! CCUserDefault::sharedUserDefault()->getBoolForKey("removeAd"))
	  {
		 	 CCDictionary* prms = CCDictionary::create();
		     prms->setObject(CCString::create("YES"), kShowBottomBannerAd);
		     prms->setObject(CCString::create("NO"), kShowTopBannerAd);
		     prms->setObject(CCString::create("NO"), kMenu);
		     prms->setObject(CCString::create("NO"), kGameOver);
		     SendMessageWithParams(string("controlAds"), prms);
	  }

	 //show banner ad id = ADMOB_BANNER_ID(GameMain.h)
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("gameplay.mp3",true);
    
    this->createGameWithHNum(hNum ,vNum);
    mainMenuSprite->setVisible(false);
    inGame->setVisible(true);
    gameOver->setVisible(false);
    this->setTouchEnabled(true);
    
    if(!touchTargetAdded)
    {
//        [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
//        CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
        touchTargetAdded=true;
    }
}
void GameMain::removAdBtnHandler()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
//	Wrapper::getInstance()->removeAds();
}
void GameMain::removeAdsFinished()
{
    CCUserDefault::sharedUserDefault()->setBoolForKey("removeAd", true);
    CCUserDefault::sharedUserDefault()->flush();
}
void GameMain::rateBtnHandler()
{
	popUpp->removeFromParentAndCleanup(true);
	CCArray * childs = gameOver->getChildren();
				for (int i = 0; i < childs->count(); i++) {
					if (dynamic_cast<CCMenu*>(childs->objectAtIndex(i)) != NULL ) {
					CCMenu* menu = (CCMenu *)childs->objectAtIndex(i);
					menu->setTouchEnabled(1);
					}
				}

	SendMessageWithParams(string("rateApp"), NULL);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
}

void GameMain::rateBtnHandlerDirect()
{
	CCDictionary *dict = CCDictionary::create();
	SendMessageWithParams(string("rateApp"), dict);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
}

void GameMain::startGame(CCNode *sender, void *data){
	startBtnHandler();
}

void GameMain::rateCallback(CCNode *sender, void *data){
	CCDictionary *dict = (CCDictionary*)data;
	CCString* str = (CCString*)dict->objectForKey("show");
	int val = str->intValue();
	if(val == 1){
		CCArray * childs = gameOver->getChildren();
				for (int i = 0; i < childs->count(); i++) {
					if (dynamic_cast<CCMenu*>(childs->objectAtIndex(i)) != NULL ) {
					CCMenu* menu = (CCMenu *)childs->objectAtIndex(i);
					menu->setTouchEnabled(0);
					}
				}
			    popUpp= CCSprite::create(Utility::getImage("pop_up").c_str());
			    popUpp->setPosition(ccp(ws.width/2,ws.height/2));
			    this->addChild(popUpp);

			    clsBtn  = createButtonWithFile(Utility::getImage("cls_btn").c_str(), menu_selector(GameMain::clsBtnHandler));
			    clsBtn->setPosition(ccp(popUpp->getContentSize().width * 0.9,popUpp->getContentSize().height * 0.9));
			    popUpp->addChild(clsBtn);

			    frateBtn  = createButtonWithFile(Utility::getImage("rate").c_str(), menu_selector(GameMain::rateBtnHandler));
			    frateBtn->setPosition(ccp((popUpp->getContentSize().width*0.7),popUpp->getContentSize().height * 0.2));
			    popUpp->addChild(frateBtn);
	}
}


void GameMain::clsBtnHandler()
{
	popUpp->removeFromParentAndCleanup(true);
	CCArray * childs = gameOver->getChildren();
			for (int i = 0; i < childs->count(); i++) {
				if (dynamic_cast<CCMenu*>(childs->objectAtIndex(i)) != NULL ) {
				CCMenu* menu = (CCMenu *)childs->objectAtIndex(i);
				menu->setTouchEnabled(1);
				}
			}
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
	CCUserDefault::sharedUserDefault()->setBoolForKey("rateDialogCancelled",false);
}

void GameMain::leaderBtnHandler()
{
	SendMessageWithParams(string("leaderBoardController"), NULL);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
}
void GameMain::moreAppsBtnHandler()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
	SendMessageWithParams(string("showMoreApps"), NULL);
//	RevMobX::sharedRevMobX()->showPopup();

}
void GameMain::restorBtnHandler()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
}
void GameMain::multiPlayerBtnHandler()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
	SendMessageWithParams(string("launchDashboard"), NULL);
//	Wrapper::getInstance()->launchNPDashboard();
}
void GameMain::popUp()
{	NDKHelper::AddSelector("GameSceneSelectors",
        "rateCallback",
        callfuncND_selector(GameMain::rateCallback),
        this);
	SendMessageWithParams(string("shouldDisplayRateDialog"), NULL);
}
void GameMain::postScoreOnLeaderboard(){
    CCDictionary* prms = CCDictionary::create();
    CCString *leaderBoardId;
    if(Utility::isGooglePlay){

            leaderBoardId = CCString::create("CgkIw8HC250BEAIQAA");
     }else{

            leaderBoardId = CCString::create("fullscore");
}
    prms->setObject(leaderBoardId, "leaderBoardId");
    CCString *leader = NULL;
    if (Utility::isAndroid) {
        leader = CCString::createWithFormat("%d",score);
    }else{
        leader = CCString::createWithFormat("%d",score);
    }
    prms->setObject(leader,"score");
    SendMessageWithParams(string("submitScore"), prms);
}
void GameMain::backHandler()
{
	SendMessageWithParams(string("reportForfeit"), NULL);
//		Wrapper::getInstance()->reportForfeitNP();
    if(! CCUserDefault::sharedUserDefault()->getBoolForKey("removeAd"))
    {
    	CCDictionary* prms = CCDictionary::create();
    	// if(!Utility::isAdFreeVersion()) {
    	prms->setObject(CCString::create("NO"), kShowBottomBannerAd);
    	prms->setObject(CCString::create("NO"), kShowTopBannerAd);
    	prms->setObject(CCString::create("NO"), kMenu);
    	prms->setObject(CCString::create("NO"), kGameOver);
    	SendMessageWithParams(string("controlAds"), prms);

//    	AdMobX::sharedAdMobX()->showFullScreen(ADMOB_MENU_FSCREEN_ID);
    }
//    AdMobX::sharedAdMobX()->hideBannerAd();

    this->unschedule(schedule_selector(GameMain::loop));
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menu.mp3", true);
    
    this->removeSingleTouch();
    if (gameOver->isVisible() == false)
    {
        inGame->setVisible(false);
        this->removeGame();
        mainMenuSprite->setVisible(true);
    }

}
void GameMain::freeBtnHandler()
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
//	AdMobX::sharedAdMobX()->showPopup();

}
void GameMain::okHandler()
{

	if(! CCUserDefault::sharedUserDefault()->getBoolForKey("removeAd"))
	{
		CCDictionary* prms = CCDictionary::create();
		// if(!Utility::isAdFreeVersion()) {
		prms->setObject(CCString::create("NO"), kShowBottomBannerAd);
		prms->setObject(CCString::create("NO"), kShowTopBannerAd);
		prms->setObject(CCString::create("NO"), kMenu);
		prms->setObject(CCString::create("NO"), kGameOver);
		SendMessageWithParams(string("controlAds"), prms);

		//		AdMobX::sharedAdMobX()->showFullScreen(ADMOB_MENU_FSCREEN_ID);
	}
//	AdMobX::sharedAdMobX()->hideBannerAd();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menu.mp3", true);

    
    this->removeSingleTouch();
    gameOver->setVisible(false);
    this->removeGame();
    inGame->setVisible(false);
    mainMenuSprite->setVisible(true);
}

void GameMain::keyBackClicked()
{
    if(gameOver->isVisible())
    {
    	this->okHandler();
    }
    else if(inGame->isVisible())
    {
    	this->backHandler();
    }
    else if(mainMenuSprite->isVisible())
    {
    	CCDirector::sharedDirector()->end();
    }
}

#pragma mark other methods
void GameMain::removeGame()
{
    if (!gameIsOver)
    {
//        [self unschedule:@selector(loop)];
        this->unschedule(schedule_selector(GameMain::loop));
    }
    gemsCMC->removeAllChildrenWithCleanup(true);
    gem2DArr->release();
    gem2DArr = CCArray::create();
    gem2DArr->retain();
    
    for (int j=0; j<vNum; j++)
    {
        CCArray *hArr=CCArray::create();
        for (int i=0; i<hNum; i++)
        {
            hArr->addObject(CCString::create("em"));
        }
        gem2DArr->addObject(hArr);
    }
}

void GameMain::loop(float dt)
{
    time-=1;
    
    if(CCDirector::sharedDirector()->getWinSizeInPixels().width >640)
    {
        timeBar->setScaleX( 2.133* 64.0f*(time/gameTime));
    }
//    else if(ws.height == 1024 && [UIScreen mainScreen].scale == 1)
//    {
//        timeBar.scaleX= sd.scaleFactorX/2* 64.0f*(time/gameTime);
//    }
    else
    {
        timeBar->setScaleX( 64.0f*(time/gameTime));
    }
    
    if (time<=0) {
    	postScoreOnLeaderboard();
    	if(CCUserDefault::sharedUserDefault()->getBoolForKey("rateDialogCancelled",true))
    	{
    	popUp();
    	}

        int gameOverCount = CCUserDefault::sharedUserDefault()->getIntegerForKey("gameOverCount",0);
        gameOverCount++;
       if(gameOverCount == CCUserDefault::sharedUserDefault()->getIntegerForKey("gameOverCountVal",1))
        {
    	   CCUserDefault::sharedUserDefault()->setIntegerForKey("gameOverCount",0);
//    	   AdMobX::sharedAdMobX()->showFullScreen(ADMOB_GAMEOVER_FSCREEN_ID);
//    	       	   AdMobX::sharedAdMobX()->hideBannerAd();

    	   CCDictionary* prms = CCDictionary::create();
    	   // if(!Utility::isAdFreeVersion()) {
    	   prms->setObject(CCString::create("NO"), kShowBottomBannerAd);
    	   prms->setObject(CCString::create("NO"), kShowTopBannerAd);
    	   prms->setObject(CCString::create("NO"), kMenu);
    	   prms->setObject(CCString::create("YES"), kGameOver);
    	   SendMessageWithParams(string("controlAds"), prms);
        }else{
        	CCUserDefault::sharedUserDefault()->setIntegerForKey("gameOverCount",gameOverCount);
        }

        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("gameover.mp3");
//        NSString *category =[[[NSBundle mainBundle] bundleIdentifier] stringByAppendingString:@".leaderboard"];
//        GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
//        scoreReporter.value = score;
//        
//        [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
//         if (error != nil)
//         {
//         NSLog(@"Submitting score error: %@", [error description]);
//         }
//         else {
//         NSLog(@"Submitting score success");
//         }
//         
//         }];
        CCDictionary *dixt = CCDictionary::create();
        dixt->setObject(CCString::createWithFormat("%d",score),"score");
        SendMessageWithParams(string("reportControlledTournamentOverWithScore"), dixt);
//        Wrapper::getInstance()->reportScoreNPGameOver(score);

        
        
        
        gameIsOver=true;
        
        gameOver->setVisible(true);
        
        scoreTextTTF2->setString(CCString::createWithFormat("%i",score)->getCString());
        
        this->unschedule(schedule_selector(GameMain::loop));
    }
}

void GameMain::createGameWithHNum(int _hNum, int _vNum)
{
    this->removeGame();
    
    gameIsOver=false;
    moveAble=true;
    time=gameTime;
    
    score=0;
    
    scoreTextTTF->setString(CCString::createWithFormat("%i",score)->getCString());
    
//    [self schedule:@selector(loop) interval:1/60.0];
    this->schedule(schedule_selector(GameMain::loop), 1/60.0);
    
    for (int j=0; j<_vNum; j++)
    {
        for (int i=0; i<_hNum; i++)
        {
            Gem *gem= this->getAGemAvoidComboWithHID(i,j);
            gem->setPosition(ccp(i*(gemWid),-j*(gemWid)));
            
            gem->indexH=i;
            gem->indexV=j;
            
            gemsCMC->addChild(gem);
            gem->setParent(gemsCMC);
            CCArray *arr= (CCArray*) gem2DArr->objectAtIndex(j);
            arr->replaceObjectAtIndex(i, gem);
            
        }
        
    }

}

Gem* GameMain::getAGemAvoidComboWithHID(int _hID, int _vID)
{
    Gem *gem;
    //NSMutableArray *allArr=[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    CCArray *debarArr=CCArray::create();
    //left
    
    int count=0;
    int toCheckID=_hID-1;
    
    
    CCString *memoryType;
    if(toCheckID>=0)
    {
        memoryType=((Gem*)(((CCArray*)gem2DArr->objectAtIndex(_vID))->objectAtIndex(toCheckID)))->type;
        
    }
    else
    {
        memoryType=CCString::create("no");
        
    }
    
    
    bool hasCombo=true;
    while (count!=2&&toCheckID>=0)
    {
        
        
        if(strcmp(memoryType->getCString(),((Gem*)(((CCArray*)gem2DArr->objectAtIndex(_vID))->objectAtIndex(toCheckID)))->type->getCString()) !=0)
//        if(memoryType != ((Gem*)(((CCArray*)gem2DArr->objectAtIndex(_vID))->objectAtIndex(toCheckID)))->type )
        {
            hasCombo=false;
        }
        toCheckID--;
        count++;
    }
    
    if(hasCombo && ( strcmp(memoryType->getCString(),"no") !=0))
    {
        debarArr->addObject(memoryType);
    }
    
    
    //top
    
    count=0;
    toCheckID=_vID-1;
    if(toCheckID>=0)
    {
        memoryType=((Gem*)((CCArray*)gem2DArr->objectAtIndex(toCheckID))->objectAtIndex(_hID))->type;
    }
    else{
        memoryType=CCString::create("no");
    }
    hasCombo=true;
    while (count!=2&&toCheckID>=0)
    {
        
//        if(memoryType!=((Gem*)((CCArray*)gem2DArr->objectAtIndex(toCheckID))->objectAtIndex(_hID))->type)
        if(strcmp(memoryType->getCString(), ((Gem*)((CCArray*)gem2DArr->objectAtIndex(toCheckID))->objectAtIndex(_hID))->type->getCString())!=0)
        {
            hasCombo=false;
        }
        toCheckID--;
        count++;
    }
    
    if(hasCombo && ( strcmp(memoryType->getCString(),"no") !=0))
    {
        if (debarArr->count() &&  strcmp(((CCString*)debarArr->objectAtIndex(0))->getCString(),memoryType->getCString())==0)
        {
            debarArr->addObject(memoryType);
        }
        else
        {
            debarArr->addObject(memoryType);
        }
    }
    
    gem = this->createGemDifferentFromArray(debarArr);
    return gem;
}

Gem* GameMain::createGemDifferentFromArray(cocos2d::CCArray *diffArr)
{
    CCArray *allArr=CCArray::create(CCString::create("1"),CCString::create("2"),CCString::create("3"),CCString::create("4"),CCString::create("5"),CCString::create("6"), NULL);
    
   
    
    for (int i=0; i<diffArr->count(); i++)
    {
//        CCLOG("%s",((CCString*)diffArr->objectAtIndex(i))->getCString());
        CCString* temp = (CCString*)diffArr->objectAtIndex(i);
        for(int j=0;j<allArr->count();j++)
        {
            if(strcmp(temp->getCString(),((CCString*)allArr->objectAtIndex(j))->getCString())==0)
                allArr->removeObjectAtIndex(j);
        }
        
    }
    
    
    
    int rn=arc4random()%allArr->count();
    CCString *str= (CCString*)allArr->objectAtIndex(rn);
    CCLOG("%s",str->getCString());
    CCSprite *gemSprite= CCSprite::create(CCString::createWithFormat(Utility::getImage("gem%s").c_str(),str->getCString())->getCString());
    Gem *gem= new Gem;
    gem->init();
    gem->type=str;
    gem->type->retain();
    gem->img=gemSprite;
    gem->img->setScale(.9);
    gem->addChild(gemSprite);
    return gem;
}

CCArray* GameMain::tryRemove()
{
    if(gameIsOver){
        return CCArray::create();
    }
    //reset
    for (int _j=0; _j<gem2DArr->count(); _j++)
    {
        CCArray *_lineArr= (CCArray*)gem2DArr->objectAtIndex(_j);
        for (int _i=0; _i<_lineArr->count(); _i++)
        {
            Gem *_gem=((Gem*)_lineArr->objectAtIndex(_i));
            _gem->checked=false;
            _gem->addedToTheRemoveList=false;
            _gem->hasToMove=false;
            _gem->memoryIndexV=_gem->indexV;
        }
    }
    
    // then  check
    
    CCArray *toRemoveArr=CCArray::create();
    
    for (int j=0; j<gem2DArr->count(); j++)
    {
        CCArray *lineArr= (CCArray*)gem2DArr->objectAtIndex(j);
        
        for (int i=0; i<lineArr->count(); i++)
        {
            Gem *gem= (Gem*)lineArr->objectAtIndex(i);
            
            if(gem->checked)
            {
                continue;
            }
            
            //left
            CCString *theType =(CCString*) gem->type;
            CCLOG("%s",theType->getCString());
            int toCheckID=i-1;
            bool same = true;
            CCArray *listArr=CCArray::create();
            
            listArr->addObject(gem);
            
            while (toCheckID>=0&&same)
            {
//                if ([theType isEqualToString:((Gem*)[lineArr objectAtIndex:toCheckID])->type])
                if(strcmp(theType->getCString(), ((Gem*)lineArr->objectAtIndex(toCheckID))->type->getCString())==0)
                {
                    listArr->addObject(gem);
                    
                }
                else
                {
                    same = false;
                }
                toCheckID--;
            }
            //right
            same = true;
            toCheckID=i+1;
            while (toCheckID<lineArr->count()&&same)
            {
                if (strcmp(theType->getCString(), ((Gem*)lineArr->objectAtIndex(toCheckID))->type->getCString())==0)
                {
                    listArr->addObject(gem);
                    
                }
                else
                {
                    same = false;
                }
                toCheckID++;
            }
            
            if (listArr->count()>=3)
            {
                for (int k=0; k<listArr->count(); k++)
                {
                    Gem *theGem=((Gem*)listArr->objectAtIndex(k));
                    if (theGem->addedToTheRemoveList==false)
                    {
                        toRemoveArr->addObject(theGem);
                        theGem->addedToTheRemoveList=true;
                    }
                }
            }
            else
            {
                listArr->removeAllObjects();
                listArr->addObject(gem);
            }
            
            int listArrMemoryCount=listArr->count();
            
            
            //top
            int sameNum=0;
            same = true;
            toCheckID=j-1;
            
            while (toCheckID>=0&&same)
            {
                
                if(strcmp(theType->getCString(), ((Gem*)((CCArray*)gem2DArr->objectAtIndex(toCheckID))->objectAtIndex(i))->type->getCString())==0)
                {
                    listArr->addObject(gem);
                    sameNum++;
                    
                }
                else
                {
                    same = false;
                }
                
                toCheckID--;
            }
            
            //bottom
            
            same = true;
            toCheckID=j+1;
            while (toCheckID<gem2DArr->count()&&same)
            {
                if(strcmp(theType->getCString(), ((Gem*)((CCArray*)gem2DArr->objectAtIndex(toCheckID))->objectAtIndex(i))->type->getCString())==0)
                {
                    listArr->addObject(gem);
                    sameNum++;
                }
                else{
                    same = false;
                }
                toCheckID++;
            }
            
            
            if (listArr->count()>=3&&sameNum>=2) {
                for (int k=listArrMemoryCount; k<listArr->count(); k++)
                {
                    Gem *theGem=((Gem*)listArr->objectAtIndex(k));
                    if (theGem->addedToTheRemoveList==false)
                    {
                        toRemoveArr->addObject(theGem);
                        theGem->addedToTheRemoveList=true;
                    }
                }
            }
            
        }
    }
    
    return toRemoveArr;
}
void GameMain::exChangeFinishedHandler()
{
    if(gameIsOver){
        return;
    }
    ___toRemoveArr=this->tryRemove();
    ___toRemoveArr->retain();
    if (___toRemoveArr->count()>0)
    {
        score+=___toRemoveArr->count()*10;
//        Wrapper::getInstance()->reportScoreNP(score);

        CCDictionary *dixt = CCDictionary::create();
        dixt->setObject(CCString::createWithFormat("%d",score),"score");
        SendMessageWithParams(string("reportScoreForCurrentTournament"), dixt);

        
        scoreTextTTF->setString(CCString::createWithFormat("%i",score)->getCString());
        for (int i=0; i<___toRemoveArr->count(); i++)
        {
            Gem *gem= (Gem*) ___toRemoveArr->objectAtIndex(i);
            
            /////// junaid //////////
            CCLabelTTF * scoreVisual = CCLabelTTF::create(CCString::createWithFormat("%d",10)->getCString(), FONT_NAME, 25*sd->scaleFactorY);
            scoreVisual->setPosition(gem->getPosition());
            gemsCMC->addChild(scoreVisual ,10);
            
            CCActionInterval * fadein = CCFadeOut::create(.5);
            
            CCCallFuncND * actionRemove = CCCallFuncND::create(this, callfuncND_selector(GameMain::removeVisualLabel), (void*)scoreVisual);
            scoreVisual->runAction(CCSequence::create(fadein,actionRemove, NULL));
            ///////////////////////////// callfuncND_selector
            
            CCActionInterval * scaleXY = CCScaleTo::create(.2, 0);
    
            CCFiniteTimeAction * moveXEase = CCEaseExponentialOut::create(scaleXY);
            CCCallFunc *cccf;
            CCSequence *seq;
            if (i==0)
            {
                cccf = CCCallFunc::create(this, callfunc_selector(GameMain::removeGemFinishedHandler));
                seq  = CCSequence::create(moveXEase,cccf,NULL);
            }
            else
            {
                seq = CCSequence::create(moveXEase,NULL);
            }
            
            ((CCArray*)gem2DArr->objectAtIndex(gem->indexV))->replaceObjectAtIndex(gem->indexH, CCString::create("empty"));
            gem->runAction(seq);
        }
        
        
        
    }
    else
    {


        CCActionInterval * moveX = CCMoveTo::create(.4, ccp(gemToBeExchanged->getPosition().x,gemToBeExchanged->getPosition().y));
        
        CCFiniteTimeAction * moveXEase = CCEaseExponentialOut::create(moveX);
        CCCallFunc *cccf=CCCallFunc::create(this, callfunc_selector(GameMain::fw_exChangeFinishedHandler));
        CCSequence *seq=CCSequence::create(moveXEase,cccf,NULL);
        
        
        
        CCActionInterval * moveX2 = CCMoveTo::create(.4, ccp(gemMoved->getPosition().x,gemMoved->getPosition().y));
        CCFiniteTimeAction * moveXEase2 = CCEaseExponentialOut::create(moveX2);
        CCCallFunc *cccf2=CCCallFunc::create(this, callfunc_selector(GameMain::fw_exChangeFinishedHandler2)); 
        CCSequence *seq2=CCSequence::create(moveXEase2,cccf2,NULL);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("error.mp3");
        gemMoved->runAction(seq);
        gemToBeExchanged->runAction(seq2);
        
    }
}

void GameMain::exChangeFinishedHandler2()
{
    
}

void GameMain::removeVisualLabel(void *aObject)
{

    CCLabelTTF * toRemove = (CCLabelTTF *)aObject;
    gemsCMC->removeChild(toRemove, true);
    
}

void GameMain::removeGemFinishedHandler()
{
    if(gameIsOver)
    {
        return;
    }
    for (int i=0; i<___toRemoveArr->count(); i++) {
        Gem *gem= (Gem*)___toRemoveArr->objectAtIndex(i);
        
        gem->getParent()->removeChild(gem,true);
    }
    ___toRemoveArr->release();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("clear.mp3");
    this->addNewGemsToTheTop();
}

void GameMain::addNewGemsToTheTop()
{
    if(gameIsOver){
        return;
    }
    for (int j=0; j<gem2DArr->count(); j++)
    {
        CCArray *lineArr= (CCArray*)gem2DArr->objectAtIndex(j);
        
        for (int i=0; i<lineArr->count(); i++)
        {
            CCObject *  aa = (lineArr->objectAtIndex(i));
            ;
//            if (![[lineArr objectAtIndex:i] isKindOfClass:[Gem class]])
            if( !dynamic_cast<Gem*>(aa))
            {
                
                int currentH=i;
                int currentV=j;
                
                for (int k=currentV-1; k>=0; k--)
                {
                    CCObject* bb = ((CCArray*)gem2DArr->objectAtIndex(k))->objectAtIndex(currentH);
                    if(dynamic_cast<Gem*>(bb))
                    {
                        Gem *gemTop=(Gem*)((CCArray*)gem2DArr->objectAtIndex(k))->objectAtIndex(currentH);
                        gemTop->indexV++;
                        gemTop->hasToMove=true;
                        
                    }
                }
                
            }
            
        }
    }
    
    CCArray *positionChangedGemsArr=CCArray::create();
    for (int _j=0; _j<gem2DArr->count(); _j++)
    {
        CCArray *lineArr= (CCArray*) gem2DArr->objectAtIndex(_j);
        
        for (int _i=0; _i<lineArr->count(); _i++) {
            
//            if ([[lineArr objectAtIndex:_i] isKindOfClass:[Gem class]])
            if(dynamic_cast<Gem*>(lineArr->objectAtIndex(_i)))
            {
                
                Gem *gem= (Gem*)lineArr->objectAtIndex(_i);
                if (gem->hasToMove)
                {
                    CCCallFunc *cccf;
                    CCSequence *seq;
                    CCActionInterval * move = CCMoveTo::create(.6, ccp(gem->getPosition().x,-gem->indexV*(gemWid)));
                    CCFiniteTimeAction * moveXEase = CCEaseExponentialOut::create(move);
                    cccf= CCCallFunc::create(this, callfunc_selector(GameMain::moveDownGemsFinishedHandler)); 
                    seq= CCSequence::create(moveXEase,cccf,NULL);
                    gem->runAction(seq);
                    positionChangedGemsArr->addObject(gem);
                    //[[gem2DArr objectAtIndex:gem->indexV] setObject:gem atIndexedSubscript:gem->indexH];
                }
            }
            
            else{
                
            }
        }
    }
    

    for (int _k=0; _k<positionChangedGemsArr->count(); _k++)
    {
        Gem *__gem=(Gem*) positionChangedGemsArr->objectAtIndex(_k);
        
        ((CCArray*)gem2DArr->objectAtIndex(__gem->memoryIndexV))->replaceObjectAtIndex(__gem->indexH,CCString::create("empty"));
    }
    

    for (int _k=0; _k<positionChangedGemsArr->count(); _k++)
    {
        Gem *__gem= (Gem*) positionChangedGemsArr->objectAtIndex(_k);
        ((CCArray*)gem2DArr->objectAtIndex(__gem->indexV))->replaceObjectAtIndex(__gem->indexH,__gem);
        
    }
    
    
    bool handlerAdded=false;
    
    

    
    CCArray *newGemsCreatedArr=CCArray::create();
    
    for (int __j=0; __j<gem2DArr->count(); __j++)
    {
        
        
        CCArray *lineArr= (CCArray*)gem2DArr->objectAtIndex(__j);
        
        for (int __i=0; __i<lineArr->count(); __i++)
        {
            if (!dynamic_cast<Gem*>(lineArr->objectAtIndex(__i)))
            {
                CCCallFunc *cccfNew;
                CCSequence *seqNew;
                Gem *newGem= this->createGemDifferentFromArray(CCArray::create());
                newGem->indexH=__i;
                newGem->indexV=__j;
                newGem->setPosition(ccp(newGem->indexH*(gemWid),-newGem->indexV*(gemWid)+(gemWid)*4));
                gemsCMC->addChild(newGem);
                
//                id moveNew=[CCMoveTo actionWithDuration:0.6 position:ccp(newGem.position.x,-newGem->indexV*gemWid)];
                CCActionInterval * moveNew = CCMoveTo::create(.6,ccp(newGem->getPosition().x,-newGem->indexV*(gemWid)));
                
//                id moveXEaseNew=[CCEaseBounceOut actionWithAction:moveNew];
                CCFiniteTimeAction * moveXEaseNew = CCEaseExponentialOut::create(moveNew);
                
                if(!handlerAdded)
                {
                    cccfNew= CCCallFunc::create(this, callfunc_selector(GameMain::newCreatedGemsMoveDownFinishedHandler));
                    
                    seqNew=CCSequence::create(moveXEaseNew,cccfNew,NULL);
                    handlerAdded=true;
                }
                else
                {
                    seqNew= CCSequence::create(moveXEaseNew,NULL);
                }
                

                newGem->runAction(seqNew);
                newGemsCreatedArr->addObject(newGem);
            }
            
            
        }
    }
    
    //重设下落的宝石，使数组对应
    for (int _k=0; _k<newGemsCreatedArr->count(); _k++)
    {
        Gem *__gem= (Gem*) newGemsCreatedArr->objectAtIndex(_k);
        ((CCArray*)gem2DArr->objectAtIndex(__gem->indexV))->replaceObjectAtIndex(__gem->indexH,__gem);
    }
    
    gemToBeExchanged=NULL;
    gemMoved=NULL;
}

void GameMain::fw_exChangeFinishedHandler()
{
    if(gameIsOver){
        return;
    }
    CCArray *hArr= (CCArray*)gem2DArr->objectAtIndex(gemToBeExchanged->indexV);
    
    hArr->replaceObjectAtIndex(gemToBeExchanged->indexH, gemMoved);

    
    CCArray *hArr2= (CCArray*)gem2DArr->objectAtIndex(gemMoved->indexV);
    
    hArr2->replaceObjectAtIndex(gemMoved->indexH,gemToBeExchanged);
    
    int tempGemMovedIndexH=gemMoved->indexH;
    int tempGemMovedIndexV=gemMoved->indexV;
    
    int tempGemToBeExchangedIndexH=gemToBeExchanged->indexH;
    int tempGemToBeExchangedIndexV=gemToBeExchanged->indexV;
    
    gemMoved->indexH=tempGemToBeExchangedIndexH;
    gemMoved->indexV=tempGemToBeExchangedIndexV;
    
    gemToBeExchanged->indexH=tempGemMovedIndexH;
    gemToBeExchanged->indexV=tempGemMovedIndexV;
    
    
    gemToBeExchanged=NULL;
    gemMoved=NULL;
    
    this->addSingleTouch();
}
void GameMain::fw_exChangeFinishedHandler2()
{
    
}

void GameMain::newCreatedGemsMoveDownFinishedHandler()
{
    gemToBeExchanged=NULL;
    gemMoved=NULL;
    
    if(gameIsOver){
        return;
    }
    ___toRemoveArr=this->tryRemove();
    ___toRemoveArr->retain();
    if (___toRemoveArr->count()>0) {
        score+=___toRemoveArr->count()*10;
//        Wrapper::getInstance()->reportScoreNP(score);
        CCDictionary *dixt = CCDictionary::create();
        dixt->setObject(CCString::createWithFormat("%d",score),"score");
        SendMessageWithParams(string("reportScoreForCurrentTournament"), dixt);
        scoreTextTTF->setString(CCString::createWithFormat("%i",score)->getCString());
        
        for (int i=0; i<___toRemoveArr->count(); i++)
        {
            Gem *gem= (Gem*)___toRemoveArr->objectAtIndex(i);
            
            /////// junaid //////////
            CCLabelTTF * scoreVisual = CCLabelTTF::create(CCString::createWithFormat("%d",10)->getCString() ,FONT_NAME ,30*sd->scaleFactorY);
            scoreVisual->setPosition(gem->getPosition());
            gemsCMC->addChild(scoreVisual,10);
            
            CCActionInterval * fadein = CCFadeOut::create(.5);
            CCCallFuncND * actionRemove = CCCallFuncND::create(this, callfuncND_selector(GameMain::removeVisualLabel), (void*)scoreVisual);
            scoreVisual->runAction(CCSequence::create(fadein,actionRemove, NULL));
            /////////////////////////////
            
            
            CCActionInterval * scaleXY = CCScaleTo::create(.2, 0);
            CCFiniteTimeAction * moveXEase = CCEaseExponentialOut::create(scaleXY);
            CCCallFunc *cccf;
            CCSequence *seq;
            if (i==0)
            {
                cccf = CCCallFunc::create(this, callfunc_selector(GameMain::removeGemFinishedHandler));
                seq  = CCSequence::create(moveXEase,cccf,NULL);
            }
            else
            {
                seq = CCSequence::create(moveXEase,NULL);
            }
            
            ((CCArray*)gem2DArr->objectAtIndex(gem->indexV))->replaceObjectAtIndex(gem->indexH, CCString::create("empty"));
            gem->runAction(seq);

        }
        
    }
    else
    {
        this->addSingleTouch();
    }

}

void GameMain::moveDownGemsFinishedHandler()
{
    
}

void GameMain::addSingleTouch()
{
    moveAble=true;
    closeAble=true;
}

# pragma mark touches methds
Gem* GameMain::getGemAtStagePointWithPoint(cocos2d::CCPoint p)
{
    for (int j=0; j<gem2DArr->count(); j++) {
        CCArray *arr= (CCArray*)(gem2DArr->objectAtIndex(j));
        
        for (int i=0; i<arr->count(); i++)
        {
            Gem *gem= (Gem*)(arr->objectAtIndex(i));
            

            float gw=gemWid/2;
            float gh=gemWid/2;

            float _x= gem->getPosition().x+gem->getParent()->getPosition().x+gem->getParent()->getParent()->getPosition().x;
            float _y=gem->getPosition().y+gem->getParent()->getPosition().y+gem->getParent()->getParent()->getPosition().y-gw*2;
            
            gem->gx=_x;
            gem->gy=_y;
            CCRect rect= CCRect(_x-gw, _y+gh, gemWid, gemWid);
            if (rect.containsPoint(p))
            {
                return gem;
            }
            
        }
    }
    
    return NULL;
}

void GameMain::ccTouchesBegan(cocos2d::CCSet *touches, cocos2d::CCEvent *event)
{
    CCTouch* touch = (CCTouch*)( touches->anyObject() );
    
    CCPoint location = CCDirector::sharedDirector()->convertToGL(touch->getLocationInView());
    
    if(gameIsOver)
    {
        return ;
    }
    CCPoint point = ccp(location.x, location.y);
    if(moveAble&&!gameIsOver)
    {
        memoryTouchedGem=this->getGemAtStagePointWithPoint(point);
        if(memoryTouchedGem)
            memoryTouchedGem->setScale(1.2);
    }
}
void GameMain::ccTouchesMoved(cocos2d::CCSet *touches, cocos2d::CCEvent *event)
{
    if(gameIsOver)
    {
        return;
    }
    CCTouch* touch = (CCTouch*)( touches->anyObject() );
    
    CCPoint point = CCDirector::sharedDirector()->convertToGL(touch->getLocationInView());
    if(memoryTouchedGem&&moveAble)
    {
        
        if(fabs(point.x-memoryTouchedGem->gx)>30*sd->scaleFactorX||fabs(point.y-(memoryTouchedGem->gy+gemWid))>30*sd->scaleFactorY)
        {
            
            Gem *gem;
            if (fabs(point.x-memoryTouchedGem->gx)>fabs(point.y-(memoryTouchedGem->gy+gemWid)))
            {
                gem=this->getGemAtStagePointWithPoint(ccp(point.x, memoryTouchedGem->gy+gemWid));
            }
            else{
                gem=this->getGemAtStagePointWithPoint(ccp(memoryTouchedGem->gx, point.y));
            }
            
            if (gem!=NULL)
            {
                
                
                gemMoved=memoryTouchedGem;
                gemToBeExchanged=gem;
                
//                [gemsCMC reorderChild:gemMoved z:[[gemsCMC children] count]-1];
                
                gemsCMC->reorderChild(gemMoved, gemsCMC->getChildren()->count()-1);
                
                //gemMoved.position=ccp(gemMoved.position.x-30,gemMoved.position.y);
                
                gemMoved->setScale(1.0);
                
                
                
//                id moveX= [CCMoveTo actionWithDuration:0.4 position:ccp(gemToBeExchanged.position.x,gemToBeExchanged.position.y)];
//                id moveXEase=[CCEaseExponentialOut actionWithAction:moveX];
                
                CCActionInterval * moveX = CCMoveTo::create(.4, ccp(gemToBeExchanged->getPosition().x,gemToBeExchanged->getPosition().y));
                CCFiniteTimeAction * moveXEase = CCEaseExponentialOut::create(moveX);
                
                
                CCCallFunc *cccf=CCCallFunc::create(this, callfunc_selector(GameMain::exChangeFinishedHandler));
                CCSequence *seq= CCSequence::create(moveXEase,cccf,NULL);
                
                
                
//                id moveX2=[CCMoveTo actionWithDuration:0.4 position:ccp(gemMoved.position.x,gemMoved.position.y)];
                CCActionInterval * moveX2 = CCMoveTo::create(.4, ccp(gemMoved->getPosition().x,gemMoved->getPosition().y));
                CCFiniteTimeAction *moveXEase2 = CCEaseExponentialOut::create(moveX2);
                
                CCCallFunc *cccf2= CCCallFunc::create(this, callfunc_selector(GameMain::exChangeFinishedHandler2));
//                CCCallFunc *cccf2=[CCCallFunc actionWithTarget:self selector:@selector(exChangeF≥inishedHandler2)];
                CCSequence *seq2=CCSequence::create(moveXEase2,cccf2,NULL);
                
                
                
                gemMoved->runAction(seq);
                gemToBeExchanged->runAction(seq2);
                
                memoryTouchedGem=NULL;
                
                CCArray *hArr= (CCArray*)gem2DArr->objectAtIndex(gemToBeExchanged->indexV);
                
                hArr->replaceObjectAtIndex(gemToBeExchanged->indexH, gemMoved);
                
                CCArray *hArr2= (CCArray*)gem2DArr->objectAtIndex(gemMoved->indexV);
                
                hArr2->replaceObjectAtIndex(gemMoved->indexH,gemToBeExchanged);
                
                int tempGemMovedIndexH=gemMoved->indexH;
                int tempGemMovedIndexV=gemMoved->indexV;
                
                int tempGemToBeExchangedIndexH=gemToBeExchanged->indexH;
                int tempGemToBeExchangedIndexV=gemToBeExchanged->indexV;
                
                gemMoved->indexH=tempGemToBeExchangedIndexH;
                gemMoved->indexV=tempGemToBeExchangedIndexV;
                
                gemToBeExchanged->indexH=tempGemMovedIndexH;
                gemToBeExchanged->indexV=tempGemMovedIndexV;
                
                
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("move.mp3");
                this->removeSingleTouch();
                
                
                
            }
            
        }
    }

    
}
void GameMain::ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *event)
{
    if(gameIsOver){
        return;
    }
    if (memoryTouchedGem!=NULL) {
        memoryTouchedGem->setScale(1);
        memoryTouchedGem=NULL;
    }
}
void GameMain::ccTouchesCancelled(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent)
{
    
}

void GameMain::removeSingleTouch()
{
    closeAble=false;
    moveAble=false;
}
