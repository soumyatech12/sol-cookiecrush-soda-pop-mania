
//
//  GameMain.h
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#ifndef __JewelMania__GameMain__
#define __JewelMania__GameMain__

#include <iostream>
#include "cocos2d.h"
#include "SharedData.h"
#include "Gem.h"

#define kShowBottomBannerAd "showbottom"
#define kShowTopBannerAd "showtop"
#define kMenu "menu"
#define kGameOver "gameover"

using namespace cocos2d;

#define ADMOB_MENU_FSCREEN_ID "ca-app-pub-3724814266830107/2222555679"
#define ADMOB_GAMEOVER_FSCREEN_ID "ca-app-pub-3724814266830107/3699288877"
#define ADMOB_BANNER_ID "ca-app-pub-3724814266830107/9745822477"

class Gem;
class GameMain : public CCLayer
{

	bool isNxtPeer;
public:
	static inline long millisecondNow()
	{
	    struct cc_timeval now;
	    CCTime::gettimeofdayCocos2d(&now, NULL);
	    return (now.tv_sec * 1000 + now.tv_usec / 1000);
	}
    static CCScene * scene();
    virtual bool init();
    
    void showFAd();
    void tournamentEnded();
    void dashboardAppear();
    void dashboardDisappear();

    static GameMain * getInstance();
    ///// methods
    // Touches
    virtual void ccTouchesBegan(CCSet* touches, CCEvent* event);
    virtual void ccTouchesEnded(CCSet* touches, CCEvent* event);
    virtual void ccTouchesMoved(CCSet* touches, CCEvent* event);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent);

    void startGame(CCNode *sender, void *data);
    virtual void keyBackClicked();
    
    CCMenu* createButtonWithFile(const char * fileName, SEL_MenuHandler _sel);
    void startBtnHandler();
    void moreAppsBtnHandler();
    void multiPlayerBtnHandler();
    void rateBtnHandler();
    void popUp();
    void postScoreOnLeaderboard();
    void leaderBtnHandler();
    void clsBtnHandler();
    void removAdBtnHandler();
    void removeAdsFinished();
    void restorBtnHandler();
    void backHandler();
    void freeBtnHandler();
    void okHandler();
    void createGameWithHNum(int _hNum ,int _vNum);
    void rateCallback(CCNode *sender, void *data);
    Gem* getAGemAvoidComboWithHID(int _hID ,int _vID);
    Gem* createGemDifferentFromArray(CCArray *diffArr);
    void removeGame();
    Gem* getGemAtStagePointWithPoint(CCPoint p);
    void exChangeFinishedHandler();
    void exChangeFinishedHandler2();
    void removeSingleTouch();
    void removeVisualLabel(void * aObject);
    void removeGemFinishedHandler();
    void fw_exChangeFinishedHandler();
    void fw_exChangeFinishedHandler2();
    void addNewGemsToTheTop();
    void moveDownGemsFinishedHandler();
    void newCreatedGemsMoveDownFinishedHandler();
    void addSingleTouch();
    CCArray* tryRemove();

    void rateBtnHandlerDirect();
    void loop(float dt);
    
    CCSize ws;
    //////
    CCSprite *mainMengBG;
    CCMenu *startBtn;
    CCMenu *moreAppsBtn;
    CCMenu *rateBtn;
    CCMenu *clsBtn;
    CCMenu *frateBtn;
    
    //////////// junaid //////////
    SharedData * sd;
    CCMenu  *multiPlayerBtn;
    CCMenu  *leaderBtn;
    CCMenu  *removAdBtn;
    CCMenu  *restoreBtn;
    CCMenu  *freeBtn;
    CCMenu  *moreGamesBtnGameOver;
    bool isMultiPlayer;
//    ALAdView *appLovinBanner;
    ///////////////////////////////
    
    
    CCSprite *mainMenuSprite;
    
    CCSprite *inGame;
    CCSprite  *popUpp;
    CCSprite *gemsCMC;
    
    CCArray *gem2DArr;
    
    Gem *memoryTouchedGem;
    
    Gem *gemMoved;
    Gem *gemToBeExchanged;
    
    float gemHei;
    float gemWid;
    
    int kindsNum;
    
    CCArray *___toRemoveArr;
    
    
    ///// junaid ///////
    CCLabelTTF * scoreTextTTF;
    CCLabelTTF * scoreTextTTF2;
    ////////////////////
    CCLabelBMFont *scoreText;
    CCLabelBMFont *scoreText2;
    
    int vNum;
    
    CCMenu *backBtn,*okBtn;
    bool closeAble;
    
    CCSprite *gameOver;
    
    CCSprite *timeBar;
    int hNum;
    
    
    int score;
    float time;
    
    bool moveAble;
    
    bool gameIsOver;
    
    float gameTime;
    
    bool touchTargetAdded;
};
#endif /* defined(__JewelMania__GameMain__) */
