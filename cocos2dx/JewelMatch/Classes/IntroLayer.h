//
//  IntroLayer.h
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#ifndef __JewelMania__IntroLayer__
#define __JewelMania__IntroLayer__

#include <iostream>
#include "cocos2d.h"
#include "SharedData.h"
using namespace cocos2d;

class IntroLayer : public CCLayer
{
public:
    SharedData * sd;
    
    static CCScene * scene();
    
    virtual void onEnter();
    void makeTransition(CCTime dt);
};
#endif /* defined(__JewelMania__IntroLayer__) */
