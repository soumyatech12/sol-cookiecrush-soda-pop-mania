//
//  Gem.cpp
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#include "Gem.h"


bool Gem::init()
{
    if(!CCSprite::init())
        return false;
    
    addedToTheRemoveList=false;
    checked=false;
    hasToMove=false;
    return true;
}