//
//  Utility.cpp
//  Zombie
//
//  Created by Redlizard on 13/02/14.
//
//

#include "Utility.h"
#include "SimpleAudioEngine.h"

#define kNumberOfCharacter 7;

using namespace CocosDenshion;
using namespace std;

bool Utility::isPad = false;
bool Utility::isGooglePlay = false;
bool Utility::isAndroid = true;
std::string Utility::folderTypeName = "";


void Utility::setBirdNumber(int number){
    CCUserDefault::sharedUserDefault()->setIntegerForKey("birdNumber", number);
}

CCString* Utility::getBirdNumber(){
    int number =  CCUserDefault::sharedUserDefault()->getIntegerForKey("birdNumber", 1);
    return CCString::createWithFormat("%d",number);
}

void Utility::setEasyMode(int easyNum){
    CCUserDefault::sharedUserDefault()->setIntegerForKey("easyNumber", easyNum);
}

int Utility::getEasyMode(){
    int easyNum =  CCUserDefault::sharedUserDefault()->getIntegerForKey("easyNumber", 0);
    return easyNum;
}

bool Utility::shouldPlaySounds(){
	bool playSounds = CCUserDefault::sharedUserDefault()->getBoolForKey(kSoundKey);
	return playSounds;
}


int Utility::playSound(CCString *sound ,float volume , bool loop){
	int soundId = -1;
    SimpleAudioEngine *soundEngine;
	if(shouldPlaySounds()){
		if (soundEngine == NULL) {
			//Get the sound engine instance, if something went wrong this will be nil
			soundEngine = SimpleAudioEngine::sharedEngine();
		}
		soundId = soundEngine->playEffect(sound->getCString(), loop);
	}
	return soundId;
}

std::string Utility::getImage(string fileName){
	CCString *finalFileName;
	if(folderTypeName.compare("iphonehd") == 0){
		finalFileName = CCString::createWithFormat("%s-hd.png",fileName.c_str());
	}else if(folderTypeName.compare("ipadhd") == 0){
		finalFileName = CCString::createWithFormat("%s-ipadhd.png",fileName.c_str());
	}else{
		finalFileName = CCString::createWithFormat("%s.png",fileName.c_str());
	}
	CCLog("%s",finalFileName->getCString());
	return finalFileName->getCString();
}

