//
//  JewelManiaAppDelegate.h
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "CCApplication.h"
#include "cocos2d.h"
using namespace cocos2d;
/**
@brief    The cocos2d Application.

The reason to implement with private inheritance is to hide some interface details of CCDirector.
*/
class  AppDelegate : private cocos2d::CCApplication
{
public:
    AppDelegate();
    virtual ~AppDelegate();

    /**
    @brief    Implement CCDirector and CCScene init code here.
    @return true    Initialize success, app continue.
    @return false   Initialize failed, app terminate.
    */
    virtual bool applicationDidFinishLaunching();

    /**
    @brief  The function is called when the application enters the background
    @param  the pointer of the application instance
    */
    virtual void applicationDidEnterBackground();

    /**
    @brief  The function is called when the application enters the foreground
    @param  the pointer of the application instance
    */
    virtual void applicationWillEnterForeground();
};
typedef struct tagResource
{
    cocos2d::CCSize size;
    char directory[100];
}Resource;

static cocos2d::CCSize designResolutionSize =    CCSizeMake(320.0f , 480.0f );

static Resource smallResource              =  { CCSizeMake(320.0f , 480.0f ),    "iphone"    };
static Resource mediumResource            =  { CCSizeMake(640.0f , 960.0f),    "iphonehd"  };
static Resource iPhone5Resource             =  { CCSizeMake(640.0f, 1136.0f),    "iphonehd"   };
static Resource iPadResource                =  { CCSizeMake(768.0f , 1024.0f),    "iphonehd"      };
//static Resource largeResource              =  { CCSizeMake(1536.0f, 2048.0f),    "ipadhd"    };
#endif // _APP_DELEGATE_H_

