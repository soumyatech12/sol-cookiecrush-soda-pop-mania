//
//  Gem.h
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#ifndef __JewelMania__Gem__
#define __JewelMania__Gem__

#include <iostream>
#include "cocos2d.h"
#include "SharedData.h"
using namespace cocos2d;

class Gem : public CCSprite
{
public:
    
    
    virtual bool init();
    
    
    CCString *type;
    CCSprite *img;
    
    bool addedToTheRemoveList;

    bool checked;
    int indexH;
    int indexV;
    bool checkedInLine;
    int memoryIndexV;
    
    bool hasToMove;
    float gx;
    float gy;
};
#endif /* defined(__JewelMania__Gem__) */
