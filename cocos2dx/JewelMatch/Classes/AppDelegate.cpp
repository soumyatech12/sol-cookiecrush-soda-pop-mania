//
//  JewelManiaAppDelegate.cpp
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "IntroLayer.h"
#include "SharedData.h"
#include "Utility.h"

USING_NS_CC;
using namespace CocosDenshion;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
    CCSize frameSize =pDirector->getWinSizeInPixels();
    // turn on display FPS
    //pDirector->setDisplayStats(true);
    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);
    
    //  ------------------------------------------------------------------------- //
    //  --------------      FOR Different Screen Resolution     ----------------- //
    //  ------------------------------------------------------------------------- //
    
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
    // Set the design resolution
    pEGLView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionExactFit);
    
    
    std::vector<std::string> searchPath;
//    if (frameSize.width > mediumResource.size.width)
//   {
//    	Utility::folderTypeName = largeResource.directory;
//        searchPath.push_back(largeResource.directory);
//        pDirector->setContentScaleFactor(largeResource.size.height/designResolutionSize.height);
//    }
   // if the frame's height is larger than the height of small resource size, select medium resource.
    if (frameSize.width > smallResource.size.width)
   {
	   Utility::folderTypeName = mediumResource.directory;
        searchPath.push_back(mediumResource.directory);
       pDirector->setContentScaleFactor(mediumResource.size.height/designResolutionSize.height);
   }
  // if the frame's height is smaller than the height of medium resource size, select small resource.
   else
   {
	   Utility::folderTypeName = smallResource.directory;
       searchPath.push_back(smallResource.directory);
       pDirector->setContentScaleFactor(smallResource.size.height/designResolutionSize.height);
    }
    searchPath.push_back("sounds");
    CCFileUtils::sharedFileUtils()->setSearchResolutionsOrder(searchPath);
    SharedData * sd = SharedData::getSharedInstance();
            sd->scaleFactorX = 1.0;
            sd->scaleFactorY = 1.0;
    // create a scene. it's an autorelease object
    CCScene *pScene = IntroLayer::scene();


    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("menu.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("gameplay.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("clear.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("button.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("move.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("error.mp3");
    SimpleAudioEngine::sharedEngine()->setEffectsVolume(1.0);
    SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(1.0);
    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->startAnimation();
    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}
