//
//  SharedData.cpp
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#include "SharedData.h"

static SharedData * instance = NULL;

SharedData* SharedData::getSharedInstance()
{
    if(instance == NULL)
    {
        instance = new SharedData;
    }
    return instance;
}