//
//  SharedData.h
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#ifndef __JewelMania__SharedData__
#define __JewelMania__SharedData__

#include <iostream>
#include "cocos2d.h"
using namespace cocos2d;
class SharedData : public CCObject
{
public:
    int m_nLevel;
    CCArray * levelInfo;
    
    float scaleFactorX;
    float scaleFactorY;
    float scaleValue;
    static SharedData* getSharedInstance();
};

#endif /* defined(__JewelMania__SharedData__) */
