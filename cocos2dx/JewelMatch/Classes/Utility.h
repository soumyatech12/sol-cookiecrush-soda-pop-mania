//
//  Utility.h
//  Zombie
//
//  Created by Redlizard on 13/02/14.
//
//

#ifndef __Zombie__Utility__
#define __Zombie__Utility__

#include <iostream>
#include "cocos2d.h"

#define kSoundKey "sound"
USING_NS_CC;
class Utility{
public:
    static bool isGooglePlay;
    static bool isAndroid;
    static bool isPad;
    static std::string folderTypeName;
    static CCDictionary *getJSONStringForObjective();
    static void initialize();
    static void setBirdNumber(int number);
    static void setEasyMode(int easy);
    static CCString *getBirdNumber();
    static int getEasyMode();
    static bool shouldPlaySounds();
    static std::string getImage(std::string fileName);
    static int playSound(CCString *sound ,float volume , bool loop);
};

#endif /* defined(__Zombie__Utility__) */
