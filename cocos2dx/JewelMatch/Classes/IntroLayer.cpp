//
//  IntroLayer.cpp
//  JewelMania
//
//  Created by Granjur on 11/11/2013.
//
//

#include "IntroLayer.h"
#include "GameMain.h"
#include "Utility.h"
//#include "AdMobX.h"

CCScene* IntroLayer::scene()
{
    CCScene* scene= CCScene::create();
    IntroLayer * layer = new IntroLayer;
    scene->addChild(layer);
    return scene;
}

void IntroLayer::onEnter()
{
    CCLayer::onEnter();
//    if(! CCUserDefault::sharedUserDefault()->getBoolForKey("removeAd"))
//   {
//    	AdMobX::sharedAdMobX()->showFullScreen(ADMOB_MENU_FSCREEN_ID);
//   }
    // ask director for the window size
	CCSize size = CCDirector::sharedDirector()->getWinSize();
    
	CCSprite *background;
	
    sd = SharedData::getSharedInstance();
	background = CCSprite::create(Utility::getImage("mainMenuBG").c_str());
    background->setAnchorPoint(ccp(0,1));
	background->setPosition(ccp(0, size.height));
	this->addChild(background);
    
    scheduleOnce(schedule_selector(IntroLayer::makeTransition), 2.0);
}


void IntroLayer::makeTransition(CCTime dt)
{
    CCTransitionFade * fd = CCTransitionFade::create(1.0, GameMain::scene(), ccBLACK);
    CCDirector::sharedDirector()->replaceScene(fd);
 
}
