LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared
LOCAL_MODULE := AmazonGamesJni

LOCAL_MODULE_FILENAME := libgame
LOCAL_MODULE_FILENAME := libAmazonGamesJni

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
					../../Classes/GameMain.cpp \
					../../Classes/Gem.cpp \
					../../Classes/IntroLayer.cpp \
					../../Classes/SharedData.cpp \
					../../Classes/Utility.cpp \
                   ../../android/jansson/dump.c \
                   ../../android/jansson/error.c \
                   ../../android/jansson/hashtable.c \
                   ../../android/jansson/load.c \
                   ../../android/jansson/memory.c \
                   ../../android/jansson/pack_unpack.c \
                   ../../android/jansson/strbuffer.c \
                   ../../android/jansson/strconv.c \
                   ../../android/jansson/utf.c \
                   ../../android/jansson/value.c \
                   ../../android/NDKHelper/NDKCallbackNode.cpp \
                   ../../android/NDKHelper/NDKHelper.cpp
                   
           
                   
                 
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
$(LOCAL_PATH)/../../libs/cocos2dx/cocoa \
$(LOCAL_PATH)/../../ios \
$(LOCAL_PATH)/../../android/NDKHelper \
$(LOCAL_PATH)/../../cocos2d-x-jsonconverter-master/cocos2d-x-jsonconverter \
$(LOCAL_PATH)/../../cocos2d-x-jsonconverter-master/cocos2d-x-jsonconverter/cJSON  \
$(LOCAL_PATH)/../../android/jansson \
$(LOCAL_PATH)/../../../


LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)