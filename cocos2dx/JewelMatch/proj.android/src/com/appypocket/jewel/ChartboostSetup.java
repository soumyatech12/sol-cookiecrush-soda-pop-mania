package com.appypocket.jewel;

import android.app.Activity;


import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.sentio.cookiecrush.R;


public class ChartboostSetup {
	static Chartboost chartboost;
	
	public static Chartboost init(Activity pActivity) {
		chartboost = Chartboost.sharedChartboost();
		if(JewelMatch.isGooglePlay){
			chartboost.onCreate(pActivity, pActivity.getString(R.string.CHARTBOOST_GOOGLE_PLAY_APP_ID), pActivity.getString(R.string.CHARTBOOST_GOOGLE_PLAY_APP_SIG), ChartboostSetup.getChartboostDelegate());		
		}else {
			chartboost.onCreate(pActivity, pActivity.getString(R.string.CHARTBOOST_AMAZON_APP_ID), pActivity.getString(R.string.CHARTBOOST_AMAZON_APP_SIG), ChartboostSetup.getChartboostDelegate());		
		}
		chartboost.startSession();
		chartboost.cacheInterstitial();
		chartboost.cacheMoreApps();
		return chartboost;
	}
	
	public static ChartboostDelegate getChartboostDelegate() {
		ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {
			@Override
			public boolean shouldDisplayInterstitial(String location) {
				return JewelMatch.showChartBoostAd;
			}
			
			@Override
			public boolean shouldRequestInterstitial(String location) {
				return JewelMatch.showChartBoostAd;
			}
			
			@Override
			public void didCacheInterstitial(String location) {
			}

			@Override
			public void didFailToLoadInterstitial(String location) {

			}

			@Override
			public void didDismissInterstitial(String location) {
				chartboost.cacheInterstitial(location);
//				JewelMatch.prevAdTime = System.currentTimeMillis();
			}

			@Override
			public void didCloseInterstitial(String location) {
//				JewelMatch.prevAdTime = System.currentTimeMillis();
			}
			
			@Override
			public void didClickInterstitial(String location) {
//				JewelMatch.prevAdTime = System.currentTimeMillis();				
			}

			@Override
			public void didShowInterstitial(String location) {
			}

			@Override
			public boolean shouldDisplayLoadingViewForMoreApps() {
				return true;
			}
			
			@Override
			public boolean shouldRequestMoreApps() {

				return true;
			}

			@Override
			public boolean shouldDisplayMoreApps() {
				return true;
			}
			
			@Override
			public void didFailToLoadMoreApps() {
			}

			@Override
			public void didCacheMoreApps() {
			}
			
			@Override
			public void didDismissMoreApps() {
			}

			
			@Override
			public void didCloseMoreApps() {
			}

			@Override
			public void didClickMoreApps() {
			}

			@Override
			public void didShowMoreApps() {
			}

			public boolean shouldRequestInterstitialsInFirstSession() {
				return true;
			}

			@Override
			public void didFailToLoadUrl(String arg0) {
			}
		};
		return chartBoostDelegate;
	}
}
