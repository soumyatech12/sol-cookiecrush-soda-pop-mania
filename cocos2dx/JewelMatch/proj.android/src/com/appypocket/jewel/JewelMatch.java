/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package com.appypocket.jewel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Map;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.Cocos2dxRenderer;
import org.json.JSONException;
import org.json.JSONObject;

import xmlwise.Plist;
import xmlwise.XmlParseException;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amazon.ags.api.AGResponseCallback;
import com.amazon.ags.api.AGResponseHandle;
import com.amazon.ags.api.AmazonGamesCallback;
import com.amazon.ags.api.AmazonGamesClient;
import com.amazon.ags.api.AmazonGamesFeature;
import com.amazon.ags.api.AmazonGamesStatus;
import com.amazon.ags.api.leaderboards.LeaderboardsClient;
import com.amazon.ags.api.leaderboards.SubmitScoreResponse;
import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdTargetingOptions;
import com.amazon.device.ads.InterstitialAd;
import com.chartboost.sdk.Chartboost;
import com.sentio.cookiecrush.R;
import com.easyndk.classes.AndroidNDKHelper;
import com.flurry.android.FlurryAgent;
import com.nextpeer.android.Nextpeer;
import com.nextpeer.android.NextpeerCocos2DX;
import com.nextpeer.android.NextpeerListener;
import com.nextpeer.android.NextpeerTournamentEndData;
import com.nextpeer.android.NextpeerTournamentStartData;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.fullscreen.RevMobFullscreen;



public class JewelMatch extends Cocos2dxActivity{
	com.amazon.device.ads.InterstitialAd amazonInterstitial;
	AdLayout adLayout;
	protected static boolean showChartBoostAd = true, showRevMobAd = false;
	protected static long prevAdTime;
	public static JewelMatch activity;
	public Chartboost chartBoost;
	private ArrayList<String> adsOrder;
	private final String AMAZON = "Amazon";
	private final String CHARTBOOST = "Chartboost";
	private final String REVMOB="Revmob";
	public RevMob revmob;	
	private RevMobFullscreen revmobFullScreen;
	boolean isRevMobAdReceived = false,shouldDisplayAd = false, isAppStartAdLoaded;
	public boolean isBillingSupported = true;
	private int RC_UNUSED = 100;
	private boolean isNextpeerActive;
	//	IAPSetup iapSetup;
	//	IabHelper mIABHelper;
	//	PurchaseObserver myObserver;
	public static final String REMOVE_ADDS_SKU = "com.hs.game.zombiedentist.removeads";
	//	public static final String REMOVE_ADDS_SKU = "android.test.purchased";
	static int IAP_TAG = 140; 
	static final String REMOVE_ADDS = "remove_ads";
	static SharedPreferences gamePrefrence;
	static boolean isGooglePlay = false;
	boolean isLargeDevice;
	private InterstitialAd interstitial;
	private boolean isAmazonAdLoaded;
    private boolean onShowAd;
	private static String APP_NAME = "com.sentio.cookiecrush";
	private static final String APP_KEY = "42336297155";
	private static final String FLURRY_KEY= "QV6T97KP2JZZB85599PJ";


	public static AmazonGamesClient agsClient;
	private int gameOverVal = 1;
	private boolean onLoadVal = false;
	AmazonGamesCallback callback;
	EnumSet<AmazonGamesFeature> agsGameFeatures;
	public LeaderboardsClient lbClient;

	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Nextpeer.initialize(this, "dced4ea269bf594fbd45fc980572306c", _nextpeerListener);
		if(!isGooglePlay) {
			
	           callback = new AmazonGamesCallback() {
					@Override
					public void onServiceNotReady(AmazonGamesStatus status) {
						
					}
					@Override
					public void onServiceReady(AmazonGamesClient amazonGamesClient) {
						agsClient = amazonGamesClient;
						
					}
				};		
				agsGameFeatures = EnumSet.of(AmazonGamesFeature.Leaderboards);
			}
		
		AndroidNDKHelper.SetNDKReciever(this);
		NextpeerCocos2DX.onCreate(this);
		activity = JewelMatch.this;
		setContentView(R.layout.main);
		
		prevAdTime = System.currentTimeMillis();

//		initallizeAdmobInterstitial();

		Cocos2dxGLSurfaceView mGLView = (Cocos2dxGLSurfaceView) findViewById(R.id.game_gl_surfaceview);

		mGLView.setEGLContextClientVersion(2);
		mGLView.setCocos2dxRenderer(new Cocos2dxRenderer());

		adsOrder = new ArrayList<String>();
		adsOrder.add(AMAZON);
		adsOrder.add(CHARTBOOST);
		adsOrder.add(REVMOB);

		new ParseTask().execute();

		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				AlarmUtil.sheduleNotificationTimer(getApplicationContext());
			}
		});

		//		new Handler().postDelayed(new Runnable() {
		//			@Override
		//			public void run() {
		//				JewelMatch.this.runOnUiThread(new Runnable() {
		//					@Override
		//					public void run() {
		//						findViewById(R.id.logolayout).setVisibility(View.GONE);
		//					}
		//				});
		//			}
		//		}, 3000);

//		if(AlarmUtil.alarmManager!=null)
//			AlarmUtil.sheduleNotificationTimer(getApplicationContext());
//		startService(new Intent(JewelMatch.this, Receiver.class));

		if(isGooglePlay){
			//			iapSetup = new IAPSetup();
			//			mIABHelper = iapSetup.mIABHelper;
		}else{
			//			myObserver = new PurchaseObserver(this);
			//			PurchasingManager.registerObserver(myObserver);
//						AdRegistration.enableTesting(true);
//						AdRegistration.enableLogging(true);
			AdRegistration.setAppKey(getString(R.string.AMAZON_APPLICATION_KEY));
			amazonInterstitial = new com.amazon.device.ads.InterstitialAd(this);
			amazonInterstitial.loadAd(new AdTargetingOptions());
			amazonInterstitial.setListener(amazonAdListener);
//			adLayout = new AdLayout(this,com.amazon.device.ads.AdSize.SIZE_600x90);
		}

		chartBoost = ChartboostSetup.init(this);
		if(isGooglePlay){
			revmob = RevMob.start(this, "540eaf8507c05e8b4f4cdf25");
		}else {
			revmob = RevMob.start(this, "540d6b6d39bb60d2065439cd");
		}		
		revmobFullScreen = revmob.createFullscreen(this, revmobAdListener);
		
	
		

		if ((getResources().getConfiguration().screenLayout &  Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {     
			isLargeDevice = true;
		}else if ((getResources().getConfiguration().screenLayout &  Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
			isLargeDevice = true;
		}else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			isLargeDevice = false;
		}else if ((getResources().getConfiguration().screenLayout &  Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
			isLargeDevice = false;
		}
		gamePrefrence = getSharedPreferences("Cocos2dxPrefsFile", MODE_PRIVATE);
		gamePrefrence.edit().putInt("gameOverCount", 0).commit();
		gamePrefrence.edit().remove("rateDialogCancelled").commit();
	}

	
	
	
	static {
		System.loadLibrary("AmazonGamesJni");	
	}

	public void controlAds(JSONObject prms) {
		Log.e("error","asdjhajkshdjkahskdjh");
		String showbottom = null;
		String showtop = null;
		try{
			showbottom = prms.getString("showbottom");
			showtop = prms.getString("showtop");			
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		removeAd();
		if(showbottom.equalsIgnoreCase("YES")) {
			showBottomAd();
		}
		if(showtop.equalsIgnoreCase("YES")) {
			showTopAd();
		}

		boolean showMenuSceneAds = false;
		boolean showGameOverSceneAds = false;		

		try{
			showMenuSceneAds = prms.getString("menu").equalsIgnoreCase("YES");	
			showGameOverSceneAds = prms.getString("gameover").equalsIgnoreCase("YES");
			if(showGameOverSceneAds || showMenuSceneAds){
				showAds();
			}
		}
		catch (JSONException e){
			e.printStackTrace();
		}	
	}


	public void showAds() {
		if(adsOrder.size() > 0){
			String adsToShow = "";
			for(int index = 0;index<adsOrder.size();index++){
				adsToShow = adsOrder.get(index);
				if(adsToShow.equalsIgnoreCase(AMAZON) && isAmazonAdLoaded){
					Log.e("error","here AMAZON");
                    onShowAd = true;
					amazonInterstitial.showAd();
					isAmazonAdLoaded = false;
					break;
				}else if(adsToShow.equalsIgnoreCase(CHARTBOOST) && chartBoost.hasCachedInterstitial()){
					onShowAd = true;
                    chartBoost.showInterstitial();
					chartBoost.cacheInterstitial();
					break;
				}else if(adsToShow.equalsIgnoreCase(REVMOB) && isRevMobAdReceived) {
					onShowAd = true;
                    revmobFullScreen.show();
					isRevMobAdReceived = false;
					break;
				}
			}
		}
		reloadAds();
	}
	
	private void reloadAds() {
		if(!chartBoost.hasCachedInterstitial()) {
			chartBoost.cacheInterstitial();
		}
	}

	RevMobAdsListener revmobAdListener = new RevMobAdsListener() {
		
		@Override
		public void onRevMobSessionNotStarted(String arg0) {
			
		}
		
		@Override
		public void onRevMobSessionIsStarted() {
			
		}
		
		@Override
		public void onRevMobEulaWasRejected() {
			
		}
		
		@Override
		public void onRevMobEulaWasAcceptedAndDismissed() {
			
		}
		
		@Override
		public void onRevMobEulaIsShown() {
		}
		
		@Override
		public void onRevMobAdReceived() {
			isRevMobAdReceived = true;
		}
		
		@Override
		public void onRevMobAdNotReceived(String arg0) {
			isRevMobAdReceived = false;
		}
		
		@Override
		public void onRevMobAdDisplayed() {
			
		}
		
		@Override
		public void onRevMobAdDismiss() {
			revmobFullScreen = revmob.createFullscreen(JewelMatch.this, revmobAdListener);
		}
		
		@Override
		public void onRevMobAdClicked() {
			
		}
	};
	
	public void showMoreApps(JSONObject prms){
		chartBoost.showMoreApps();
	}

	public void removeAd() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				LinearLayout layoutBottom = (LinearLayout) findViewById(R.id.bottom);
				layoutBottom.removeAllViews();
				LinearLayout layoutTop = (LinearLayout) findViewById(R.id.top);
				layoutTop.removeAllViews();
			}
		});
	}

	private void attachAd(final LinearLayout layout) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(isGooglePlay){
				}
				else{
					if (adLayout == null) {
//						AdTargetingOptions adTargetingOptions = new AdTargetingOptions();
//						adLayout.setLayoutParams(new LayoutParams(600, 90));
//						adLayout.loadAd(adTargetingOptions);
//						layout.addView(adLayout);
					}else {
//						layout.addView(adLayout);
					}

				}
			}
		});
	}

	public static Object cppCall_logsth(){
		return activity;
	}

	public void showTopAd() {
		removeAd();
		LinearLayout layout = (LinearLayout) findViewById(R.id.top);
		attachAd(layout);
	}

	public void showBottomAd() {
		removeAd();
		LinearLayout layout = (LinearLayout) findViewById(R.id.bottom);
		attachAd(layout);
	}

	public void startPuchaseFlow(JSONObject prms){
		//		if(isGooglePlay){
		//			mIABHelper.launchPurchaseFlow(this, REMOVE_ADDS_SKU, 110, iapSetup);
		//		}else{
		//			showPurchaseIteamDialog(REMOVE_ADDS_SKU, REMOVE_ADDS);
		//		}
	}

	public void shareButtonClicked(JSONObject prms){
		String scoreString = null;
		int score = 0;
		try{
			scoreString = prms.getString("bestScore");
			score = Integer.parseInt(scoreString);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/html");
		if(isGooglePlay){
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>I scored "+ score+" in Floopy Bird</p> https://play.google.com/store/apps/details?id=com.effectualgaming.flyingbird"));
		}else{
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>I scored "+ score+" in Floopy Bird</p> http://www.amazon.com/Effectual-Gaming-Private-Limited-Floopy/dp/B00IKHFE7Y/ref=sr_1_1?s=mobile-apps&ie=UTF8&qid=1394101131&sr=1-1&keywords=floopy+bird"));
		}
		startActivity(Intent.createChooser(sharingIntent,"Share using"));
	}
	public void rateButtonClicked(JSONObject prms){
		Uri uri;
		if(isGooglePlay){
			uri = Uri.parse("market://details?id=" + getPackageName());
		}else{
			uri = Uri.parse("amzn://apps/android?p=" + getPackageName());
		}
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
		}
	}

	//----------------------------Amazon In - App ------------------------------------

	//	public void showPurchaseIteamDialog(final String SKU, final String key) {
	//		runOnUiThread(new Runnable() {
	//			@Override
	//			public void run() {
	//				final SharedPreferences settings = myObserver.getSharedPreferencesForCurrentUser();
	//				boolean entitled = settings.getBoolean(key, false);
	//				if(!entitled){
	//					String requestId = PurchasingManager.initiatePurchaseRequest(SKU);
	//					myObserver.storeRequestId(SKU, key);
	//				}else{
	//					Toast.makeText(getApplicationContext(), "ALREADY PURCHASED", 5000).show();
	//					setAdsFreeVersion();
	//				}
	//			}
	//		});
	//	}




	//	-----------------------------------Google play Game Service methods------------- //
	public JewelMatch() {
		super();

	}

	public JewelMatch(int requestedClients) {
		super();

	}



	@Override
	protected void onStart() {
		super.onStart();
		Nextpeer.onStart();
		chartBoost.onStart(this);
		FlurryAgent.onStartSession(this, FLURRY_KEY);
        onShowAd = false;
		rateClicked = false;
		if(gamePrefrence.contains("pause")) {
			showAds();	
			gamePrefrence.edit().remove("pause").commit();
		}
	}


	@Override
	protected void onStop() {
		super.onStop();
		Log.e("error","onStop");
		chartBoost.onStop(this);
		if(!isNextpeerActive && !onShowAd && onLoadVal && !rateClicked) {
			gamePrefrence.edit().putBoolean("pause", true).commit();
		}
		 if (Nextpeer.isCurrentlyInTournament()) { 
		        Nextpeer.reportForfeitForCurrentTournament(); 
		  }
		FlurryAgent.onEndSession(this);
	}



	@Override
	protected void onResume() {
		super.onResume();
		Log.e("error","resume");
		Cocos2dxHelper.onResume();
		if(!isGooglePlay) {
			AmazonGamesClient.initialize(this, callback, agsGameFeatures);
		}        
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.e("error","pause");
		Cocos2dxHelper.onPause();
		if (agsClient != null) {
			agsClient.release();
		}
	}

	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {

		// If an interstitial is on screen, close it. Otherwise continue as normal.
		if (chartBoost.onBackPressed())
			return;
		else
			super.onBackPressed();
	}


	public void submitScore(final String leaderboardId, final int score) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(isGooglePlay) {
//					if(mHelper.isSignedIn()) {
//						Games.Leaderboards.submitScore(getApiClient(), leaderboardId, score);
//
//
					}else {
//						
									lbClient = agsClient.getLeaderboardsClient();
									AGResponseHandle<SubmitScoreResponse> handle = lbClient.submitScore(leaderboardId, score);
									handle.setCallback(new AGResponseCallback<SubmitScoreResponse>() {			
							    	    @Override
							    	    public void onComplete(SubmitScoreResponse result) {
							    	        if (result.isError()) {
							    	         // Add optional error handling here.  Not required since re-tries and on-device request caching are automatic
							    	     
							    	        } else {
							    	         
							    	        }
							    	    }
							    	});	
								}
			}
		});

	}

	public void submitScore(JSONObject prms) {
		String leaderboardId = null;
		String scoreString = null;
		int score = 0;
		try{
			leaderboardId = prms.getString("leaderBoardId");
			scoreString = prms.getString("score");
			score = Integer.parseInt(scoreString);
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		submitScore(leaderboardId, score);
	}
	

	public void unlockAchievement (final String achId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Editor editor = gamePrefrence.edit();
				if(!gamePrefrence.getBoolean(achId, false)){
					editor.putBoolean(achId, true);
					editor.commit();
				}
				//				if(isGooglePlay) {
				//					myScoreloop.achieve(activity, achId);
				////					if(mHelper.getGamesClient().isConnected()){
				////						getGamesClient().unlockAchievement(achId);		
				////					}					
				//				}else {
				//					acClient = agsClient.getAchievementsClient();
				//					
				//					AGResponseHandle<UpdateProgressResponse> handle = acClient.updateProgress(achId, 100.0f);
				//					  
				//					// Optional callback to receive notification of success/failure.
				//					handle.setCallback(new AGResponseCallback<UpdateProgressResponse>() {
				//					  
				//					   @Override
				//					   public void onComplete(UpdateProgressResponse result) {
				//					       if (result.isError()) {
				//					           // Add optional error handling here.  Not strictly required
				//					           // since retries and on-device request caching are automatic.
				//					       } else {
				//					           // Continue game flow.
				//					       }
				//					   }
				//					});
				//				}
			}
		});
	}

	public void unlockAchievement (JSONObject prms) {
		String achievementID = null;
		try{
			achievementID = prms.getString("unlockId");
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		unlockAchievement(achievementID);
	}

	public void achievementController(JSONObject prms){
		//		if(isGooglePlay) {
		//			
		//		}else {
		//			if(agsClient != null){
		//				acClient = agsClient.getAchievementsClient();
		//				acClient.showAchievementsOverlay();
		//			}
		//		}

	}

	public void leaderBoardController(JSONObject prms){
		if(isGooglePlay) {
			Log.e("error","google gameplay");
//			if(mHelper.isSignedIn()) {
//				startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()), RC_UNUSED);
//			}else {
//				Toast.makeText(this, "Login first in order to view leaderboards.", Toast.LENGTH_LONG).show();
//				mHelper.beginUserInitiatedSignIn();
//			}
		}else {
						if(agsClient != null){
							lbClient = agsClient.getLeaderboardsClient();
							lbClient.showLeaderboardsOverlay();   
						}			
		}
	}
	public JSONObject getDict() {
		String jsonStr = "{\"fake_dictionary\":\"have_fun\"}";
		JSONObject prmsToSend = null;
		try{
			prmsToSend = new JSONObject(jsonStr);
		}
		catch (JSONException e){
			e.printStackTrace();
		} 
		return prmsToSend;
	}
	public void setAdsFreeVersion() {
		removeAd();
		AndroidNDKHelper.SendMessageWithParameters("removeAdsListener", getDict());
	}

	
	public void shouldDisplayRateDialog(JSONObject prms){
		boolean dateVal =   System.currentTimeMillis() >= gamePrefrence.getLong("rateDate", 0)
				+ (3 * 24 * 60 * 60 * 1000);
		
		JSONObject prmsToSend = new JSONObject();
		try {
			if(dateVal){
				prmsToSend.put("show", "1");
			}else{
				prmsToSend.put("show", "0");
			}
			
			AndroidNDKHelper.SendMessageWithParameters("rateCallback", prmsToSend);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public void resetGame(JSONObject prms) {
		AlertDialog.Builder exitbuilder = new AlertDialog.Builder(this);
		exitbuilder.setTitle("Floopy Birf")
		.setMessage(R.string.RESET_MESSAGE)
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				gamePrefrence.edit().clear();
				gamePrefrence.edit().commit();
				AndroidNDKHelper.SendMessageWithParameters("resetListener", getDict());
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog stopalert = exitbuilder.create();
		stopalert.show();
	}

	public void quitGame(JSONObject prms) {
		AlertDialog.Builder exitbuilder = new AlertDialog.Builder(this);
		exitbuilder.setTitle(getString(R.string.app_name))
		.setMessage(R.string.QUIT_MESSAGE)
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				AndroidNDKHelper.SendMessageWithParameters("resetListener", getDict());
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog stopalert = exitbuilder.create();
		stopalert.show();
	}

	@Override 
	public synchronized void onWindowFocusChanged(final boolean pHasWindowFocus) {
		super.onWindowFocusChanged(pHasWindowFocus);
		if (!pHasWindowFocus) {
			return;
		}
		Handler h = new Handler();
		Runnable r = new Runnable() {
			@Override
			public void run() {
				AndroidNDKHelper.SendMessageWithParameters("resumeBackgroundMusic", getDict());
			}
		};
		h.post(r);
	}


//	private void initallizeAdmobInterstitial() {
//		interstitial = new InterstitialAd(this);
//		interstitial.setAdUnitId("a1534696a773f73");
//		interstitial.setAdListener(adListener);
//		interstitial.loadAd(new AdRequest.Builder().build());
//
//	}




//	public void showAdmobInterestial(boolean prms){
//		if(prms) {
//			if(interstitial.isLoaded()) {
//				interstitial.show();
//			}
//		}		
//	}


	public void showAmazonInterestial(boolean prms){
		if(prms) {
			if(isAmazonAdLoaded) {
				amazonInterstitial.showAd();
				isAmazonAdLoaded = false;
			}
		}		
	}
	private boolean rateClicked;
	public  void rateApp(final JSONObject val) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if(val == null){
					Editor edit = gamePrefrence.edit();
					edit.putLong("rateDate",System.currentTimeMillis());
					edit.commit();
				}
				rateClicked = true;
				JewelMatch.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("amzn://apps/android?p="+APP_NAME)));
			}
		});

	}
	private class BackgroundTask extends AsyncTask<String, Void, String>	{ 
		@Override
		protected String doInBackground(String...param) {
			Log.e("error in doinbackground","show");
			return DownloadFromUrl();		
		}

		public String DownloadFromUrl()  {
			String s2 = null;;
			try {
				String fileName=null;
				URL url = new URL("http://adification.com/amazon_jewelblitz.plist");

				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								url.openStream()));

				String inputLine;
				String str="";

				while ((inputLine = in.readLine()) != null){
					Log.e("error",inputLine);
					str+=inputLine;
				}

				parsePlist(str);
				in.close();
			}catch (Exception e) {
				e.printStackTrace();
				Log.e("error","here");
			}
			return s2;   
		}
	}
	private void parsePlist(String str)
	{	       
		Map<String, Object> properties = null;
		try {
			properties = Plist.fromXml(str);
			String[] adsArray = new String[5];
			Map<String, Object> inner_properties = ConfigHelpers.GetDictForKey("Ads", properties);
			adsArray[Integer.valueOf(ConfigHelpers.GetValue(AMAZON,inner_properties)) - 1] = AMAZON;
			adsArray[Integer.valueOf(ConfigHelpers.GetValue(CHARTBOOST,inner_properties)) - 1] = CHARTBOOST;
			adsArray[Integer.valueOf(ConfigHelpers.GetValue(REVMOB,inner_properties)) - 1] = REVMOB;
			
			adsOrder.clear();
			for(int i =0; i<=4;i++){
				adsOrder.add(adsArray[i]);
			}
			String gameOverString = ConfigHelpers.GetValue("GameOvers",properties);
			gameOverVal  = (Integer.valueOf(gameOverString));

			gamePrefrence.edit().putInt("gameOverCountVal", gameOverVal).commit();
			
			String blackhatVal = ConfigHelpers.GetValue("onLoad",properties);
			onLoadVal  = (blackhatVal.equalsIgnoreCase("true"))?true:false;
			
		} catch (XmlParseException e) {
			e.printStackTrace();
		}
	}
	
	private class ParseTask extends AsyncTask<String, Void, Void>{
		@Override
		protected Void doInBackground(String...param) {
			Log.e("error in parsetask","show");
			return DownloadUrl();		
		}
		 
		public Void DownloadUrl()  {
			try {
				URL url = new URL("http://adification.com/1c86528ff62246bdb86ae4c48c3baac3.plist");

				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								url.openStream()));
				
				String inputLine; 
				String str="";

				while ((inputLine = in.readLine()) != null){
					Log.e("error",inputLine);
					str+=inputLine;
				}
				
				parsePlist(str);
				in.close();
			}catch (Exception e) {
				e.printStackTrace();
				new BackgroundTask().execute();
			}
			return null;
		}
	}
//	com.google.android.gms.ads.AdListener adListener = new com.google.android.gms.ads.AdListener() {
//		@Override
//		public void onAdLoaded() {
//			isAmazonAdLoaded = true;
//		}
//
//		@Override
//		public void onAdFailedToLoad(int errorCode) {
//			isAmazonAdLoaded = false;
//		}
//
//		@Override
//		public void onAdOpened() {
//
//		}
//
//		@Override
//		public void onAdClosed() {
//			interstitial.loadAd(new AdRequest.Builder().build());
//			prevAdTime = System.currentTimeMillis();
//		}
//
//		@Override
//		public void onAdLeftApplication() {
//			prevAdTime = System.currentTimeMillis();
//		}
//	};


	AdListener amazonAdListener = new AdListener() {

		@Override
		public void onAdLoaded(Ad arg0, AdProperties arg1) {
			Log.e("error","success");
			isAmazonAdLoaded = true;
		}

		@Override
		public void onAdFailedToLoad(Ad arg0, AdError arg1) {
			Log.e("error",arg1.getMessage());
			isAmazonAdLoaded = false;
			amazonInterstitial.loadAd(new AdTargetingOptions());
		}

		@Override
		public void onAdExpanded(Ad arg0) {

		}

		@Override
		public void onAdDismissed(Ad arg0) {
			amazonInterstitial.loadAd();
			prevAdTime = System.currentTimeMillis();
		}

		@Override
		public void onAdCollapsed(Ad arg0) {
			amazonInterstitial.loadAd(new AdTargetingOptions());
			prevAdTime = System.currentTimeMillis();
		}
	};

	// function for checking internet connectivity
	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) 
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) 
				for (int i = 0; i < info.length; i++) 
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}
		}   
		return false;
	}

	 private NextpeerListener _nextpeerListener = new NextpeerListener() 
	    {
	        public void onTournamentStart(NextpeerTournamentStartData startData) {
	        	AndroidNDKHelper.SendMessageWithParameters("startGame", getDict());
	        }

	        @Override
			public void onNextpeerAppear() {
	        	Log.e("error","appear");
	        	super.onNextpeerAppear();
	        	isNextpeerActive = true;
			}

			@Override
			public void onNextpeerDisappear() {
				Log.e("error","onNextpeerDisappear");
				super.onNextpeerDisappear();
				isNextpeerActive = false;
			}

			public void onTournamentEnd(NextpeerTournamentEndData endData) {
				
			}
	    };
	    
	    public void launchDashboard(JSONObject obj){
	    	 Nextpeer.launch();
	    }
	    
	    public void reportForfeit(JSONObject obj){
	    	 if (Nextpeer.isCurrentlyInTournament()) { 
	    	        Nextpeer.reportForfeitForCurrentTournament(); 
	    	    }
	    }
	    
	    public void reportControlledTournamentOverWithScore(JSONObject obj){
	    	try{
	    		String str = obj.getString("score");
	    		int score = Integer.valueOf(str);
	    		Nextpeer.reportControlledTournamentOverWithScore(score);
	    	}catch(JSONException ex){
	    		ex.printStackTrace();
	    	}
	    }
	    
	    public void reportScoreForCurrentTournament(JSONObject obj){
	    	try{
	    		String str = obj.getString("score");
	    		int score = Integer.valueOf(str);
	    		Nextpeer.reportScoreForCurrentTournament(score);
	    	}catch(JSONException ex){
	    		ex.printStackTrace();
	    	}
	    }
}
