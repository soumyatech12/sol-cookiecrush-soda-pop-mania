package com.appypocket.jewel;

import java.util.Map;

public class ConfigHelpers {

	 public static String GetValue(String key, Map<String, Object> dict)

	    {

	          String result = "";

	try{

	               for (Map.Entry<String,Object> entry : dict.entrySet()) {

	                    if(key.equals(entry.getKey()))  {

	                          result = entry.getValue().toString();

	                          break;

	                    }

	               }   

	          }

	    catch(Exception ex)

	    {
	    	ex.printStackTrace();

	    }

	          return result;

	    }

	//Helps to read another key value as another dictionary

	    @SuppressWarnings("unchecked")

	    public static Map<String, Object> GetDictForKey(String key, Map<String, Object> dict)

	    {

	          Map<String, Object> result = null;

	          try{

	               for (Map.Entry<String,Object> entry : dict.entrySet()) {

	                    if(key.equals(entry.getKey())) {

	                          result = (Map<String, Object>)entry.getValue();

	                          break;

	                    }

	               }   

	               return result;

	          }

	          catch(Exception ex)

	          {

	        	  ex.printStackTrace();
	          }

	          return result;

	    }
	}
