package com.appypocket.jewel;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmUtil {
	public static AlarmManager alarmManager;
	public static void sheduleNotificationTimer(Context context) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(1);
		alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	    Intent intent = new Intent(context, com.appypocket.jewel.Receiver.class);

	    PendingIntent pintent = PendingIntent.getService(context, 123456789,
	            intent, 0);


	    Calendar calNow = Calendar.getInstance();
        Calendar calSet = (Calendar) calNow.clone();

        calSet.set(Calendar.HOUR_OF_DAY, 19);
        calSet.set(Calendar.MINUTE, 30);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);

        if (calSet.compareTo(calNow) <= 0) {
            
            calSet.add(Calendar.DATE, 1);
        }


	    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
	    		calSet.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pintent);
	}
}
