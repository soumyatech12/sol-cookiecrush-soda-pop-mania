package com.appypocket.jewel;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.sentio.cookiecrush.R;

public class Receiver extends IntentService{

	public Receiver() {
		super("Hello");
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int  onStartCommand(Intent intent, int flags,int startId) {
		super.onStart(intent, startId);
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		PendingIntent pendingIntent = PendingIntent.getActivity(Receiver.this, 0, new Intent(Receiver.this, JewelMatch.class), 0);
		
		final Notification notification = new Notification(R.drawable.icon, "Jewel Mania", System.currentTimeMillis());
		notification.setLatestEventInfo(Receiver.this, "Jewel Mania", "Time for some matching fun!", pendingIntent);
		
		notificationManager.notify(1, notification);
		return START_STICKY;

	}

	@Override
	public void onDestroy() {
		// / TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onHandleIntent(Intent intent) {

	}
}